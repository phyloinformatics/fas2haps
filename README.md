# READ ME

## Name

fas2haps.R

## Description

Reads a mult-FASTA file with a DNA sequence alignment and returns some data about haplotype diversity.

## Author

- Name: Denis Jacob Machado, Ph.D. (ORDIC: [0000-0001-9858-4515](https://orcid.org/0000-0001-9858-4515))
- Institution: [University of North Carolina at Charlotte](https://www.charlotte.edu/)
- Homepage: [phyloinformatics.com](http://phyloinformatics.com/)

## Dependencies

Operating system: Tested on macOS Monterey, v12.6. Should work in other Unix-based systems and maybe Windows (good luck).
Other software: R (Rscript), version 4+, with ape and pegas.

## Input

Multi-FASTA alignment (DNA sequences)

## Example command line

```bash
$ Rscript pegas.R example.fasta
```

## Expected output

- `example.fasta.log`: Log with some execution notes
- `example.fasta.pdf`: Simple haplotype network (for visual inspection only)
- `example.fasta.names.csv`: Table indicating which sequence belongs to each haplotype
- `example.fasta.hapdiv.csv`: Haplotive Diversity (Hd), following Nei (1987)
